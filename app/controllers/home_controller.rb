class HomeController < ApplicationController
  def index
    if current_user
      gon.user = {:id => current_user['id'], :email => current_user['email']}
    end
  end
end
