var rtc = ('undefined' === typeof module ? {} : module.exports);
(function() {
    // Initial Setup
    // -------------
    (function (exports, global) {

        rtc.VERSION = '0.0.1';

        rtc.createStream = function(opts) {
            var peer = null;
            if (opts && opts.url) {
                peer = new rtc.PeerConnection(opts).init();
            }
            return peer;
        }
    })('object' === typeof module ? module.exports : (this.rtc = {}), this);

    // Component - util from Socket.IO
    (function (exports, global) {

        /**
         * Utilities namespace.
         *
         * @namespace
         */

        var util = exports.util = {};

        /**
         * Merges two objects.
         *
         * @api public
         */

        util.merge = function merge (target, additional, deep, lastseen) {
            var seen = lastseen || []
                , depth = typeof deep == 'undefined' ? 2 : deep
                , prop;

            for (prop in additional) {
                if (additional.hasOwnProperty(prop) && util.indexOf(seen, prop) < 0) {
                    if (typeof target[prop] !== 'object' || !depth) {
                        target[prop] = additional[prop];
                        seen.push(additional[prop]);
                    } else {
                        util.merge(target[prop], additional[prop], depth - 1, seen);
                    }
                }
            }

            return target;
        };

        /**
         * Merges prototypes from objects
         *
         * @api public
         */

        util.mixin = function (ctor, ctor2) {
            util.merge(ctor.prototype, ctor2.prototype);
        };

        /**
         * Checks if the given object is an Array.
         *
         *     rtc.util.isArray([]); // true
         *     rtc.util.isArray({}); // false
         *
         * @param Object obj
         * @api public
         */

        util.isArray = Array.isArray || function (obj) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        };

        /**
         * Array indexOf compatibility.
         *
         * @see bit.ly/a5Dxa2
         * @api public
         */

        util.indexOf = function (arr, o, i) {

            for (var j = arr.length, i = i < 0 ? i + j < 0 ? 0 : i + j : i || 0;
                 i < j && arr[i] !== o; i++) {}

            return j <= i ? -1 : i;
        };

    })('undefined' != typeof rtc ? rtc : module.exports, this);

    // Component - Event Emitter from Socket.IO
    (function (exports, rtc) {

        /**
         * Expose constructor.
         */

        exports.EventEmitter = EventEmitter;

        /**
         * Event emitter constructor.
         *
         * @api public.
         */

        function EventEmitter () {};

        /**
         * Adds a listener
         *
         * @api public
         */

        EventEmitter.prototype.on = function (name, fn) {
            if (!this.$events) {
                this.$events = {};
            }

            if (!this.$events[name]) {
                this.$events[name] = fn;
            } else if (rtc.util.isArray(this.$events[name])) {
                this.$events[name].push(fn);
            } else {
                this.$events[name] = [this.$events[name], fn];
            }

            return this;
        };

        EventEmitter.prototype.addListener = EventEmitter.prototype.on;

        /**
         * Adds a volatile listener.
         *
         * @api public
         */

        EventEmitter.prototype.once = function (name, fn) {
            var self = this;

            function on () {
                self.removeListener(name, on);
                fn.apply(this, arguments);
            };

            on.listener = fn;
            this.on(name, on);

            return this;
        };

        /**
         * Removes a listener.
         *
         * @api public
         */

        EventEmitter.prototype.removeListener = function (name, fn) {
            if (this.$events && this.$events[name]) {
                var list = this.$events[name];

                if (rtc.util.isArray(list)) {
                    var pos = -1;

                    for (var i = 0, l = list.length; i < l; i++) {
                        if (list[i] === fn || (list[i].listener && list[i].listener === fn)) {
                            pos = i;
                            break;
                        }
                    }

                    if (pos < 0) {
                        return this;
                    }

                    list.splice(pos, 1);

                    if (!list.length) {
                        delete this.$events[name];
                    }
                } else if (list === fn || (list.listener && list.listener === fn)) {
                    delete this.$events[name];
                }
            }

            return this;
        };

        /**
         * Removes all listeners for an event.
         *
         * @api public
         */

        EventEmitter.prototype.removeAllListeners = function (name) {
            if (name === undefined) {
                this.$events = {};
                return this;
            }

            if (this.$events && this.$events[name]) {
                this.$events[name] = null;
            }

            return this;
        };

        /**
         * Gets all listeners for a certain event.
         *
         * @api publci
         */

        EventEmitter.prototype.listeners = function (name) {
            if (!this.$events) {
                this.$events = {};
            }

            if (!this.$events[name]) {
                this.$events[name] = [];
            }

            if (!rtc.util.isArray(this.$events[name])) {
                this.$events[name] = [this.$events[name]];
            }

            return this.$events[name];
        };

        /**
         * Emits an event.
         *
         * @api public
         */

        EventEmitter.prototype.emit = function (name) {
            if (!this.$events) {
                return false;
            }

            var handler = this.$events[name];
            if (!handler) {
                return false;
            }

            var args = Array.prototype.slice.call(arguments, 1);

            if ('function' == typeof handler) {
                handler.apply(this, args);
            } else if (rtc.util.isArray(handler)) {
                var listeners = handler.slice();

                for (var i = 0, l = listeners.length; i < l; i++) {
                    listeners[i].apply(this, args);
                }
            } else {
                return false;
            }

            return true;
        };

    })('undefined' != typeof rtc ? rtc : module.exports, 'undefined' != typeof rtc ? rtc : module.parent.exports);

    // Component PeerConnection
    (function(exports, rtc, global){

        exports.PeerConnection = PeerConnection;

        var evts = {
                local: {
                    objCreate: '',
                    objExist: 'pc exist',
                    getMediaSuccess: 'get media success',
                    getMediaError: 'get media error',
                    pcReady: 'pc ready',
                    pcInitError: '',
                    pcCreateError: '',
                },
                remote: {
                    pcStreamAdd: 'pc stream add',
                    pcStreamRemove: 'pc stream remove'
                }
            },
            pcInstance,
            stunServer,
            sdpConstraints = {
                mandatory: {
                    OfferToReceiveAudio: true,
                    OfferToReceiveVideo: true
                }
            },
            pc_constraints = {
                optional: [
                    {DtlsSrtpKeyAgreement: true},
                    {RtpDataChannels: true }
                ]
            };

        if (webrtcDetectedBrowser == 'chrome') {
            stunServer = 'stun:stun.l.google.com:19302'
        } else if (webrtcDetectedBrowser == 'firefox') {
            stunServer = 'stun:23.21.150.121'
        }
        //Singleton Pattern by using outside pcInstance varialbe
        function PeerConnection(opts) {
            if (!(this instanceof PeerConnection)) {
                return new PeerConnection(opts);
            }
            if (!pcInstance) {
                pcInstance = this;
                pcInstance.emit(evts.local.objCreate);
            } else {
                pcInstance.emit(evts.local.objExist);
            }
            var socket = null,
                stream = pcInstance.stream,
                pc = null;
            pcInstance.opts = {
                audio: true,
                video: {
                    mandatory: {},
                    optional: []
                },
                iceServers: [{url: stunServer}],
                url: '',
            };
            rtc.util.merge(pcInstance.opts, opts, 0);
            return pcInstance;
        }

        rtc.util.mixin(PeerConnection, rtc.EventEmitter);

        PeerConnection.prototype.init = function () {
            var self = pcInstance,
                _opts = self.opts,
                socket = null;
            try {
                if (!self.stream) {
                    getUserMedia({audio: _opts.audio, video: _opts.video}, function(stream) {
                        self.stream = stream;
                        self.emit(evts.local.getMediaSuccess, stream);
                        if (self.stream && self.socket) {
                            self.emit(evts.local.pcReady)
                        }
                    }, function(e) {
                        self.emit(evts.local.getMediaError, e);
                    });
                }
                if (!self.socket) {
                    socket = io.connect(_opts.url)
                    socket.on('connect', function() {
                        self.socket = socket;
                        if (self.stream && self.socket) {
                            self.emit(evts.local.pcReady)
                        }
                    })
                    socket.on('offer', function(data) {
                        console.log('receive offer')
                        self.createPeerConnection();
                        self.sendAnswer(data.sdp);
                    })
                    socket.on('answer', function(data) {
                        console.log('receivce answer');
                        self.receiveAnswer(data.sdp);
                    })
                    socket.on('candidate', function(data) {
                        self.receiveCandidate(data);
                    })
                    socket.on('close', function() {
                        self.remoteClose();
                    })
                }
            } catch (e) {
                self.emit(evts.local.pcInitError, e)
            }
            return self;
        }

        PeerConnection.prototype.createPeerConnection = function () {
            var self = pcInstance,
                _opts = self.opts,
                pc = null;
            try {
                pc = new RTCPeerConnection({iceServers: _opts.iceServers}, pc_constraints);
            } catch (e) {
                console.log(e)
                self.emit(evts.local.pcCreateError, e)
                return;
            }
            pc.onicecandidate = function (event) {
                if (event.candidate) {
                    self.socket.emit('candidate', {
                        label: event.candidate.sdpMLineIndex,
                        id: event.candidate.sdpMid,
                        candidate: event.candidate.candidate
                    });
                }
                self.emit(evts.local.iceCandidate, event.candidate);
            };
            pc.onaddstream = function (event) {
                console.log('add remote stream')
                self.emit(evts.remote.pcStreamAdd, event.stream);
            };
            pc.onremovestream = function (event) {
                self.emit(events.remote.pcStreamRemove, event);
            };
            pc.addStream(self.stream);

            pc.ondatachannel = function (e) {
                var channel = e.channel || e;
                self.addDataChannel(channel);
            };
            self.pc = pc;
        }

        PeerConnection.prototype.connect = function () {
            var self = pcInstance;
            self.createPeerConnection();
            self.createDataChannel();
            self.sendOffer();
        }

        PeerConnection.prototype.createDataChannel = function () {
            var self = pcInstance,
                pc = self.pc,
                channel = null;
            try {
                channel = pc.createDataChannel("test", {reliable: false});
            } catch (e) {
                console.log(e)
            }
            self.addDataChannel(channel);
        }
        PeerConnection.prototype.addDataChannel = function (channel) {
            var self = pcInstance;

            channel.onopen = function (e) {
                self.emit(evts.remote.dcOpen);
            };

            channel.onclose = function (event) {
                console.log(event);
                self.emit(evts.remote.dcClose);
            };

            channel.onmessage = function (message) {
                console.log(message);
                self.emit(evts.remote.dcMessage);
            };

            channel.onerror = function (err) {
                console.log(err);
                self.emit(evts.remote.dcError);
            };
            pcInstance.dataChannel = channel;
            rtc.dataChannel = channel;
        }


        PeerConnection.prototype.sendOffer = function () {
            var self = pcInstance,
                pc = self.pc,
                constraints = {
                    optional: [],
                    mandatory: {
                        MozDontOfferDataChannel: true
                }
            };
            // temporary measure to remove Moz* constraints in Chrome
            if (navigator.webkitGetUserMedia) {
                for (var prop in constraints.mandatory) {
                    if (prop.indexOf("Moz") != -1) {
                        delete constraints.mandatory[prop];
                    }
                }
            }
            rtc.util.merge(constraints, sdpConstraints, 0);

            pc.createOffer(function(sessionDescription) {
                sessionDescription.sdp = preferOpus(sessionDescription.sdp);
                pc.setLocalDescription(sessionDescription);
                self.socket.emit('offer', {
                    sdp: sessionDescription
                });
            }, null, constraints);
        }

        PeerConnection.prototype.sendAnswer = function (sdp) {
            var self = pcInstance,
                pc = self.pc;
            pc.setRemoteDescription(new RTCSessionDescription(sdp));
            pc.createAnswer(function(sessionDescription) {
                pc.setLocalDescription(sessionDescription);
                self.socket.emit('answer', {
                    sdp: sessionDescription
                });
            }, null, sdpConstraints)
        }

        PeerConnection.prototype.receiveAnswer = function (sdp) {
            var self = pcInstance,
                pc = self.pc;
            pc.setRemoteDescription(new RTCSessionDescription(sdp));
        };

        PeerConnection.prototype.receiveCandidate = function (data) {
            var self = pcInstance,
                pc = self.pc;
            var candidate = new RTCIceCandidate({sdpMLineIndex:data.label, candidate:data.candidate});
            pc.addIceCandidate(candidate);
        }

        PeerConnection.prototype.close = function () {
            var self = pcInstance,
                pc = self.pc;
            self.emit(evts.local.pcClose);
            pc.close();
            pc = null;
            self.socket.emit('close')
        }

        PeerConnection.prototype.remoteClose = function () {
            var self = pcInstance,
                pc = self.pc;
            self.emit(evts.remote.pcClose);
            pc.close();
            pc = null;
        }

        function preferOpus(sdp) {
            var sdpLines = sdp.split('\r\n');

            // Search for m line.
            for (var i = 0; i < sdpLines.length; i++) {
                if (sdpLines[i].search('m=audio') !== -1) {
                    var mLineIndex = i;
                    break;
                }
            }
            if (mLineIndex === null)
                return sdp;

            // If Opus is available, set it as the default in m line.
            for (var i = 0; i < sdpLines.length; i++) {
                if (sdpLines[i].search('opus/48000') !== -1) {
                    var opusPayload = extractSdp(sdpLines[i], /:(\d+) opus\/48000/i);
                    if (opusPayload)
                        sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], opusPayload);
                    break;
                }
            }

            // Remove CN in m line and sdp.
            sdpLines = removeCN(sdpLines, mLineIndex);

            sdp = sdpLines.join('\r\n');
            return sdp;
        }

        function extractSdp(sdpLine, pattern) {
            var result = sdpLine.match(pattern);
            return (result && result.length == 2)? result[1]: null;
        }

        function setDefaultCodec(mLine, payload) {
            var elements = mLine.split(' ');
            var newLine = new Array();
            var index = 0;
            for (var i = 0; i < elements.length; i++) {
                if (index === 3) // Format of media starts from the fourth.
                    newLine[index++] = payload; // Put target payload to the first.
                if (elements[i] !== payload)
                    newLine[index++] = elements[i];
            }
            return newLine.join(' ');
        }

        // Strip CN from sdp before CN constraints is ready.
        function removeCN(sdpLines, mLineIndex) {
            var mLineElements = sdpLines[mLineIndex].split(' ');
            // Scan from end for the convenience of removing an item.
            for (var i = sdpLines.length-1; i >= 0; i--) {
                var payload = extractSdp(sdpLines[i], /a=rtpmap:(\d+) CN\/\d+/i);
                if (payload) {
                    var cnPos = mLineElements.indexOf(payload);
                    if (cnPos !== -1) {
                        // Remove CN payload from m line.
                        mLineElements.splice(cnPos, 1);
                    }
                    // Remove CN line in sdp
                    sdpLines.splice(i, 1);
                }
            }

            sdpLines[mLineIndex] = mLineElements.join(' ');
            return sdpLines;
        }
    })('undefined' != typeof rtc ? rtc : module.exports, 'undefined' != typeof rtc ? rtc : module.parent.exports, this);
})();